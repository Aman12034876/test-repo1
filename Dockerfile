# Docker Image which is used as foundation to create
# a custom Docker Image with this Dockerfile
FROM node:14

# A directory within the virtualized Docker environment
# Becomes more relevant when using Docker Compose later
RUN mkdir -p /test_repo
WORKDIR /test_repo

# Copies package.json and package-lock.json to Docker environment
#COPY package*.json ./

# Installs all node packages
#RUN npm install

# Copies everything over to Docker environment
COPY . /test_repo/

# Uses port which is used by the actual application
EXPOSE 9090
RUN npm install

RUN npm install -g serve

RUN yarn build

CMD serve -s build -p 9090
# Finally runs the application
#CMD [ "serve", "-s", "build" "-p" "6060" ]